# Utilities

## OpenStack clients

This container provides the python based OpenStack CLI. You need a container
runtime to use the utility. To use it, given you have podman installed, this is
a helpful alias:

```bash
alias o='openstack'

function openstack {
    CONF=".config/openstack"
    if [[ -z "$UID" ]]; then
        UID="$(id --user)"
    fi
    if [[ -z "$GID" ]]; then
        GID="$(id --group)"
    fi
    podman run \
        --user "${UID}:${GID}" \
        --mount type=bind,source="$HOME/$CONF",target="/$CONF" \
        --volume "${PWD}":"${PWD}" \
        --volume "${PWD}/.local" \
        --workdir "${PWD}" \
        --env-file <(env | grep '^OS_') \
        --env CLIFF_FIT_WIDTH=1 \
        --rm -it \
        gitlab.mpcdf.mpg.de/fberg/utils/openstack-client "$@"
}
```
